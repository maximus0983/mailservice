package com.getjavajob.mandzhievm.repository;

import com.getjavajob.mandzhievm.common.JmsAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JmsAccountRepository extends MongoRepository<JmsAccount, String> {
}
