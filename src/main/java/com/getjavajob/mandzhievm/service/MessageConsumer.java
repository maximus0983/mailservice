package com.getjavajob.mandzhievm.service;

import com.getjavajob.mandzhievm.common.JmsAccount;
import com.getjavajob.mandzhievm.repository.JmsAccountRepository;
import com.getjavajob.mandzhievm.repository.SequenceGeneratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {
    private static final Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

    @Autowired
    private JmsAccountRepository jmsAccountRepository;
    @Autowired
    private EmailServiceImpl emailService;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    @JmsListener(destination = "friendRequestQueue")
    public void receiveMessage(JmsAccount jmsAccount) {
        logger.debug("message {}", jmsAccount.getLastName());
        jmsAccount.setId(sequenceGeneratorService.generateSequence(jmsAccount.SEQUENCE_NAME));
        jmsAccountRepository.save(jmsAccount);
        String textMessage = "User " + jmsAccount.getName() + " made a friend request";
        emailService.sendEmail(jmsAccount.getEmailTo(), jmsAccount.getName(), textMessage);
    }
}